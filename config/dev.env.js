module.exports = {
    NODE_ENV: '"development"',
    ENV_CONFIG: '"dev"',
    BASE_API: '"http://localhost:8090"',
    APP_ORIGIN: '"http://localhost:8090"',
    IS_CORS: true
}
