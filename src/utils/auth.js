import Cookies from 'js-cookie'
import store from '@/store'

const system = 'Admin';
const Sid = 'JSESSIONID';
const Token = system + 'Token';
const RememberMe = 'remember-me';
const Remember = system + 'Remember';

export{
  Sid,
  Token,
  Remember
}

export function getSid() {
  return Cookies.get(Sid);
}

export function setSid(sid) {
  return Cookies.set(Sid, sid);
}

export function removeSid() {
  return Cookies.remove(Sid)
}

export function getToken() {
  return store.getters.token || Cookies.get(Token);
}

export function setToken(token) {
  return Cookies.set(Token, token);
}

export function removeToken() {
  return Cookies.remove(Token)
}

export function getRememberMe() {
  return Cookies.get(RememberMe);
}

export function setRememberMe(rememberme) {
  return Cookies.set(RememberMe, rememberme);
}

export function removeRememberMe() {
  return Cookies.remove(RememberMe)
}

export function getRemember() {
  return store.getters.remember || Cookies.get(Remember);
}

export function setRemember(remember) {
  return Cookies.set(Remember, remember);
}

export function removeRemember() {
  return Cookies.remove(Remember)
}

export function setCookies(name, value, path) {
  return Cookies.set(name, value, {path: path ? "/temp" : path });
}

export function hasUrlPermission(permission, role, mcode) {
  if (role === 2) return true; // 超级admin权限 直接通过
  if (!mcode) return true;
  return permission.some(code => mcode === code);
}

export function hasPermission(mcode) {
  return hasUrlPermission(store.getters.permission, store.getters.role,mcode);
}
